export type Link = { id: string; text: string; url: string };
export type Item = {
  id: string;
  text: string;
  order: number;
  keyword_links: any;
};
export type Experience = {
  id: string;
  text: string;
  role?: string;
  location?: string;
  start_date: string;
  end_date?: string;
  date_text?: string;
  items: Item[];
};
export type Section = {
  id: string;
  text: string;
  purpose: string;
  experiences: Experience[];
};
export type ResumeData = {
  id: string;
  name: string;
  nickname: string;
  title: string;
  links: Link[];
  sections: Section[];
};
