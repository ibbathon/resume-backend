/* @refresh reload */
import { render } from "solid-js/web";

import "./index.css";
import App from "./App";
import { StoreProvider } from "./Store";

render(
  () => (
    <StoreProvider store={{ todos: [] }}>
      <App />
    </StoreProvider>
  ),
  document.getElementById("root") as HTMLElement,
);
