import { fetchDefaultResume } from "../actions";
import { Component, createResource, For } from "solid-js";
import "./Resume.css";

export const Resume: Component = () => {
  const [resumeData] = createResource(fetchDefaultResume);
  return (
    <>
      {resumeData.loading && <span>"Loading..."</span>}
      {!resumeData.loading && (
        <>
          <header>
            <span id="fullname">{resumeData()?.name}</span>
            <span id="nickname">{resumeData()?.nickname}</span>
            <span id="jobtitle">{resumeData()?.title}</span>
          </header>
          <ul id="externallinks">
            <For each={resumeData()?.links}>
              {(link) => (
                <li>
                  <a href={link.url}>{link.text}</a>
                </li>
              )}
            </For>
          </ul>

          <For each={resumeData()?.sections}>
            {(section) => (
              <section data-purpose={section.purpose}>
                <h1>{section.text}</h1>
                <ul>
                  <For each={section.experiences}>
                    {(experience) => (
                      <li>
                        <ul class="experience_header">
                          <li class="organization">{experience.text}</li>
                          {experience.role && (
                            <li class="job_title">{experience.role}</li>
                          )}
                          {experience.location && (
                            <li class="location">{experience.location}</li>
                          )}
                        </ul>
                        <ul class="date_range">
                          {experience.date_text ? (
                            <li class="date_text">{experience.date_text}</li>
                          ) : (
                            <li class="date_from">{experience.start_date}</li>
                          )}
                          {experience.end_date && (
                            <li class="date_to">{experience.end_date}</li>
                          )}
                        </ul>
                        <ul class="items">
                          <For each={experience.items}>
                            {(item) => <li>{item.text}</li>}
                          </For>
                        </ul>
                      </li>
                    )}
                  </For>
                </ul>
              </section>
            )}
          </For>
        </>
      )}
    </>
  );
};
