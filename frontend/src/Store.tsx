import {
  createEffect,
  createSignal,
  createContext,
  useContext,
  JSX,
  Context,
  Accessor,
  Setter,
} from "solid-js";
import { Todo } from "./models/Todo";

type StoreData = { todos: Todo[] };
type StoreProviderProps = { store: StoreData; children: JSX.Element };
type StoreContextProps = ReturnType<typeof defineStoreMethods>;

function defineStoreMethods(
  store: Accessor<StoreData>,
  setStore: Setter<StoreData>,
) {
  return {
    todos: () => store().todos,
    setTodos: (newTodos: Todo[]) => {
      setStore({ ...store, todos: newTodos });
    },
  };
}

export function StoreProvider(props: StoreProviderProps) {
  const [store, setStore] = createSignal(props.store);
  if (localStorage.store) setStore(JSON.parse(localStorage.store));
  createEffect(() => (localStorage.store = JSON.stringify(store())));

  const storeMethods = defineStoreMethods(store, setStore);

  return (
    <StoreContext.Provider value={storeMethods}>
      {props.children}
    </StoreContext.Provider>
  );
}

const StoreContext =
  createContext<StoreContextProps>() as Context<StoreContextProps>;

export function useStore() {
  return useContext<StoreContextProps>(StoreContext);
}
