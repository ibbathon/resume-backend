from resume.models import Experience, Item, Link, Resume, Section


def run():
    Resume.objects.all().delete()
    r = Resume.objects.create(name="Dev Elopment", nickname="Dev", title="Sir")

    # Links
    Link.objects.create(resume=r, text="gitlab", url="https://gitlab.com")
    Link.objects.create(resume=r, text="github", url="https://github.com")

    # Work Experience
    s = Section.objects.create(
        resume=r, text="Work Experience", purpose="workexperience"
    )
    e = Experience.objects.create(
        section=s,
        text="Ruby Org",
        role="Macguffin",
        location="Onsite",
        start_date="1980-05-17",
        end_date="1990-12-23",
    )
    for order, text in enumerate(
        (
            "Sat on table for long periods of time.",
            "Enticed random strangers to exciting adventure.",
            "Took a very very very very very very very very very very very very very "
            + "very very very very very very very very very very very very very very "
            + "very very very very very very very very very very very very very very "
            + "very very very very very very very very very very very very very very "
            + "very very very very very very very long time to resolve.",
            "Ultimately meant nothing.",
        )
    ):
        Item.objects.create(experience=e, text=text, order=order)

    e = Experience.objects.create(
        section=s,
        text="NASA",
        role="Mascot",
        location="Space",
        start_date="1991-10-10",
        end_date="2000-01-12",
    )
    for order, text in enumerate(
        (
            "Took a very small step.",
            "Took a longer step.",
            "Threw up in the training chamber.",
            "Was loved by all.",
        )
    ):
        Item.objects.create(experience=e, text=text, order=order)

    e = Experience.objects.create(
        section=s, text="Python", role="Snake Wrangler", start_date="2000-02-23"
    )
    for order, text in enumerate(("Took over the world.",)):
        Item.objects.create(experience=e, text=text, order=order)

    e = Experience.objects.create(
        section=s,
        text="Star Tribune",
        role="Paperboy",
        start_date="1975-05-15",
        date_text="Summers of 1975, 1976, 1977",
    )

    # Skills
    s = Section.objects.create(resume=r, text="Skills", purpose="skills")
    e = Experience.objects.create(
        section=s, text="Interpersonal", start_date="2000-12-01"
    )
    for order, text in enumerate(
        (
            "great decoration",
            "easy personality",
            "not a camel",
        )
    ):
        Item.objects.create(experience=e, text=text, order=order)
    e = Experience.objects.create(section=s, text="Computer", start_date="2000-11-01")
    for order, text in enumerate(
        (
            "Excel",
            "Vim",
            "Python",
        )
    ):
        Item.objects.create(experience=e, text=text, order=order)

    # Education
    s = Section.objects.create(resume=r, text="Education", purpose="education")
    e = Experience.objects.create(
        section=s,
        text="Fake University",
        location="Space",
        start_date="1992-01-15",
        end_date="1995-05-27",
    )
    Item.objects.create(experience=e, text="B.B.S. in Physics, 1995")

    # Projects
    s = Section.objects.create(resume=r, text="Personal Projects", purpose="projects")
    e = Experience.objects.create(
        section=s, text="Thumb-twiddling", start_date="2000-12-01"
    )
    for order, data in enumerate(
        (
            (
                "Personal twiddling: focused on thumb speed.",
                {"Personal twiddling": "https://tvtropes.org"},
            ),
            ("Twiddling tutoring", {}),
        )
    ):
        Item.objects.create(
            experience=e, text=data[0], keyword_links=data[1], order=order
        )
