from django.db import models

from resume.models import Experience, Section
from resume.models.base import UUIDModel
from tests.assertions import assert_field_basic_settings


class TestSectionModel:
    class TestHappyPath:
        def test_all_fields_match(self):
            correct_fields = sorted(
                [
                    "id",
                    "created_at",
                    "updated_at",
                    "resume_id",
                    "text",
                    "purpose",
                ]
            )
            model_fields = sorted(
                [
                    f.attname
                    for f in Section._meta.get_fields()
                    if type(f).__name__ not in ["ManyToOneRel", "ManyToManyRel"]
                ]
            )
            assert model_fields == correct_fields

        def test_related_models(self):
            assert Section.experience_set.rel.related_model == Experience

        def test_inherits_from_base(self):
            assert Section.__base__ == UUIDModel

        def test_correct_field_settings(self):
            assert_field_basic_settings(
                Section,
                [
                    ["resume", models.ForeignKey, False, False],
                    ["text", models.TextField, False, False],
                    ["purpose", models.CharField, False, False],
                ],
            )
            assert Section.purpose.field.max_length == 255

        def test_str_returns_name(self, a_resume):
            section = Section(resume=a_resume, text="atext", purpose="apurpose")
            assert str(section) == "atext"
