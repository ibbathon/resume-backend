from django.db import models

from resume.models import Experience, Item
from resume.models.base import UUIDModel
from tests.assertions import assert_field_basic_settings


class TestExperienceModel:
    class TestHappyPath:
        def test_all_fields_match(self):
            correct_fields = sorted(
                [
                    "id",
                    "created_at",
                    "updated_at",
                    "section_id",
                    "text",
                    "role",
                    "location",
                    "start_date",
                    "end_date",
                    "date_text",
                ]
            )
            model_fields = sorted(
                [
                    f.attname
                    for f in Experience._meta.get_fields()
                    if type(f).__name__ not in ["ManyToOneRel", "ManyToManyRel"]
                ]
            )
            assert model_fields == correct_fields

        def test_related_models(self):
            assert Experience.item_set.rel.related_model == Item

        def test_inherits_from_base(self):
            assert Experience.__base__ == UUIDModel

        def test_correct_field_settings(self):
            assert_field_basic_settings(
                Experience,
                [
                    ["section", models.ForeignKey, False, False],
                    ["text", models.TextField, False, False],
                    ["role", models.TextField, False, True],
                    ["location", models.TextField, False, True],
                    ["start_date", models.DateField, False, False],
                    ["end_date", models.DateField, True, True],
                    ["date_text", models.TextField, False, True],
                ],
            )

        def test_str_returns_name(self, a_section):
            experience = Experience(
                section=a_section, text="atext", start_date="2022-01-01"
            )
            assert str(experience) == "atext"

        def test_reorder_shifts_intermediate_numbers(self, a_section):
            experiences = [
                Experience(section=a_section, text="text1", start_date="2022-01-01"),
                Experience(section=a_section, text="text3", start_date="2022-01-03"),
                Experience(section=a_section, text="text2", start_date="2022-01-02"),
            ]
            tuple(n.save() for n in experiences)

            assert tuple(e.text for e in a_section.experience_set.all()) == (
                "text3",
                "text2",
                "text1",
            )
