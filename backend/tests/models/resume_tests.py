from django.db import models

from resume.models import Link, Resume, Section
from resume.models.base import UUIDModel
from tests.assertions import assert_field_basic_settings


class TestResumeModel:
    class TestHappyPath:
        def test_all_fields_match(self):
            correct_fields = sorted(
                [
                    "id",
                    "created_at",
                    "updated_at",
                    "name",
                    "nickname",
                    "title",
                ]
            )
            model_fields = sorted(
                [
                    f.attname
                    for f in Resume._meta.get_fields()
                    if type(f).__name__ not in ["ManyToOneRel", "ManyToManyRel"]
                ]
            )
            assert model_fields == correct_fields

        def test_related_models(self):
            assert Resume.link_set.rel.related_model == Link
            assert Resume.section_set.rel.related_model == Section

        def test_inherits_from_base(self):
            assert Resume.__base__ == UUIDModel

        def test_correct_field_settings(self):
            assert_field_basic_settings(
                Resume,
                [
                    ["name", models.CharField, False, False],
                    ["nickname", models.CharField, False, True],
                    ["title", models.CharField, False, False],
                ],
            )
            assert Resume.name.field.max_length == 255
            assert Resume.nickname.field.max_length == 255
            assert Resume.title.field.max_length == 255

        def test_str_returns_name(self):
            resume = Resume(name="aname", title="atitle")
            assert str(resume) == "aname"
