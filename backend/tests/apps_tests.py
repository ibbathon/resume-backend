from resume.apps import ResumeConfig


class TestResumeConfig:
    class TestHappyPath:
        def test_attributes(self):
            assert ResumeConfig.name == "resume"
