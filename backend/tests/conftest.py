import pytest  # noqa:F401

pytest_plugins = [
    "tests.fixtures.resume",
    "tests.fixtures.section",
    "tests.fixtures.experience",
    "tests.fixtures.item",
    "tests.fixtures.link",
]
