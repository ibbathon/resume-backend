import pytest

from tests.helpers.client import create_client


class TestHealthcheckView:
    def test_get_health_without_db_returns_unhealthy_db(self):
        url = "/api/v1/health/"
        client = create_client()
        response = client.get(url)

        assert response.json() == {
            "api": "ok",
            "db": (
                'RuntimeError: Database access not allowed, use the "django_db" mark,'
                + ' or the "db" or "transactional_db" fixtures to enable it.'
            ),
        }

    @pytest.mark.django_db
    def test_get_health_with_db_returns_healthy_db(self):
        url = "/api/v1/health/"
        client = create_client()
        response = client.get(url)

        assert response.json() == {"api": "ok", "db": "ok"}
