from uuid import uuid4

import pytest

from tests.helpers.client import create_client


class TestResumeView:
    @pytest.mark.django_db
    def test_get_resume_with_valid_id_returns_nested_json(
        self,
        a_full_resume,
        a_link,
        a_section,
        an_experience,
        a_second_experience,
        an_item,
        url=None,
    ):
        if not url:
            url = f"/api/v1/resumes/{a_full_resume.id}/"
        client = create_client()
        response = client.get(url)

        assert response.json() == {
            "id": str(a_full_resume.id),
            "name": "Mr. Resume",
            "nickname": "Sir",
            "title": "Lord",
            "links": [
                {
                    "id": str(a_link.id),
                    "text": "gitlab",
                    "url": "https://gitlab.com/ibbathon/resume",
                },
            ],
            "sections": [
                {
                    "id": str(a_section.id),
                    "text": "a section",
                    "purpose": "a-purpose",
                    "experiences": [
                        {
                            "id": str(a_second_experience.id),
                            "text": "a minimal experience",
                            "role": "",
                            "location": "",
                            "start_date": "2022-09-23",
                            "end_date": None,
                            "date_text": "",
                            "items": [],
                        },
                        {
                            "id": str(an_experience.id),
                            "text": "a full experience",
                            "role": "a role",
                            "location": "a location",
                            "start_date": "2022-09-22",
                            "end_date": "2022-10-22",
                            "date_text": "a date text",
                            "items": [
                                {
                                    "id": str(an_item.id),
                                    "text": "an item",
                                    "keyword_links": {"item": "item.com"},
                                    "order": 1,
                                },
                            ],
                        },
                    ],
                },
            ],
        }

    @pytest.mark.django_db
    def test_get_resume_with_invalid_id_returns_404(self):
        url = f"/api/v1/resumes/{uuid4()}/"
        client = create_client()
        response = client.get(url)

        assert response.status_code == 404
        assert response.json() == {"detail": "Not found."}

    def test_get_resume_with_non_uuid_returns_400(self):
        url = "/api/v1/resumes/not-a-uuid/"
        client = create_client()
        response = client.get(url)

        assert response.status_code == 400
        assert response.json() == {"detail": "Invalid UUID."}

    @pytest.mark.django_db
    def test_get_resume_base_endpoint_returns_first_resume(
        self,
        a_full_resume,
        a_link,
        a_section,
        an_experience,
        a_second_experience,
        an_item,
    ):
        self.test_get_resume_with_valid_id_returns_nested_json(
            a_full_resume,
            a_link,
            a_section,
            an_experience,
            a_second_experience,
            an_item,
            url="/api/v1/resumes/",
        )
