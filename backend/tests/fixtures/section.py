import pytest

from resume.models import Section


@pytest.fixture
def a_section(db, a_resume):
    return Section.objects.create(
        resume=a_resume,
        text="a section",
        purpose="a-purpose",
    )
