import pytest

from resume.models import Item


@pytest.fixture
def an_item(db, an_experience):
    return Item.objects.create(
        experience=an_experience,
        text="an item",
        keyword_links={"item": "item.com"},
    )
