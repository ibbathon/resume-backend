import pytest

from resume.models import Link


@pytest.fixture
def a_link(db, a_resume):
    return Link.objects.create(
        resume=a_resume,
        text="gitlab",
        url="https://gitlab.com/ibbathon/resume",
    )
