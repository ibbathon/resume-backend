import pytest

from resume.models import Experience


@pytest.fixture
def an_experience(db, a_section):
    return Experience.objects.create(
        section=a_section,
        text="a full experience",
        role="a role",
        location="a location",
        start_date="2022-09-22",
        end_date="2022-10-22",
        date_text="a date text",
    )


@pytest.fixture
def a_second_experience(db, a_section):
    return Experience.objects.create(
        section=a_section,
        text="a minimal experience",
        start_date="2022-09-23",
    )
