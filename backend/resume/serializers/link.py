from rest_framework import serializers

from resume.models import Link


class LinkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Link
        fields = ["id", "text", "url"]
