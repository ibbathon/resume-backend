from rest_framework import serializers

from resume.models import Item


class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = ["id", "text", "order", "keyword_links"]
