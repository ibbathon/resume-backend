from rest_framework import serializers

from resume.models import Section
from resume.serializers.experience import ExperienceSerializer


class SectionSerializer(serializers.ModelSerializer):
    experiences = ExperienceSerializer(
        many=True, read_only=True, source="experience_set"
    )

    class Meta:
        model = Section
        fields = ["id", "text", "purpose", "experiences"]
