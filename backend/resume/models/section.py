from django.db import models

from resume.models.base import UUIDModel


class Section(UUIDModel):
    resume = models.ForeignKey("Resume", on_delete=models.CASCADE)
    text = models.TextField(blank=False)
    purpose = models.CharField(max_length=255, blank=False)

    def __str__(self):
        return str(self.text)
