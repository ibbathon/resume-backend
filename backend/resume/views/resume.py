from django.core.exceptions import ValidationError
from django.shortcuts import get_object_or_404
from rest_framework import status, viewsets
from rest_framework.response import Response

from resume.models import Resume
from resume.serializers import ResumeSerializer


class ResumeViewSet(viewsets.ViewSet):
    def list(self, request):
        return self.retrieve(request, Resume.objects.first().id)

    def retrieve(self, request, pk=None):
        try:
            resume = get_object_or_404(Resume.objects, pk=pk)
        except ValidationError:
            return Response(
                {"detail": "Invalid UUID."},
                status=status.HTTP_400_BAD_REQUEST,
            )
        return Response(ResumeSerializer(resume).data)
