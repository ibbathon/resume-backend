from django.db import connection
from rest_framework.response import Response
from rest_framework.views import APIView


class Healthcheck(APIView):
    """
    Unauth view to quickly check if the system is healthy.
    """

    def get(self, request, format=None):
        """
        Returns {"api": "ok", "db": "ok"} if the system is functioning.
        Otherwise returns exception messages as values.
        """
        db_status = "ok"
        try:
            connection.ensure_connection()
        except Exception as e:
            db_status = f"{type(e).__name__}: {str(e)}"
        return Response({"api": "ok", "db": db_status})
